let http = require('http');

let port = 4000;

let courses = [
	{
		"course 101": "Introduction to HTML",
		"instructor": "Rupert Ramos"
	},
	{
    "course 102": "Introduction to CSS",
		"instructor": "Rupert Ramos"
	},
  {
    "course 103": "Introduction to JS",
		"instructor": "Rupert Ramos"
	}
];

let server = http.createServer(function(request, response) {
	
	if(request.url == '/' && request.method == 'GET') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Welcome to Booking System')
	} else if(request.url == '/' && request.method == 'POST') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Welcome to Booking System')
	}

  else if(request.url == '/profile' && request.method == 'GET') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Welcome to your Profile!')
	}else if(request.url == '/profile' && request.method == 'POST') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Welcome to your Profile!')
	}

  else if(request.url == '/courses' && request.method == 'GET') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Here is our courses available')
	}else if(request.url == '/courses' && request.method == 'POST') { 
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Here is our courses available')
	}

  else if(request.url == '/addcourse' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(JSON.stringify(courses))
		response.end()
	} else if (request.url == '/addcourse' && request.method == 'POST') {
		let request_body = ' '

		request.on('data', function(data){
			request_body += data
			 
		})
		request.on('end', function() {
			console.log(typeof request_body)

			// request_body = JSON.parse(request_body)

			let new_course = {
        "course 104": "Introduction to MongoDB",
        "instructor": "Rupert Ramos"
			}
			
			courses.push(new_course)
			console.log(courses)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(new_course))
			response.end()

		})
	}

  
	
}).listen(port);



console.log(`Server is running at localhost: ${port}`);